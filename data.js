function getBirthdays() {
    return chrome.storage.sync.get(['birthdays']);
}

function setBirthdays(bdays) {
    return chrome.storage.sync.set({ 'birthdays': bdays }).then((result) => {
        console.log('Set birthdays to', bdays);
    });
}

async function deleteBirthday(name, date) {
    return await getBirthdays().then((bdays) => {
        var filtered = bdays.birthdays.filter((item) => {
            return item.name != name || item.birthdate != date;
        });

        return setBirthdays(filtered);
    });
}

async function addBirthday(name, date) {
    return await chrome.storage.sync.get(['birthdays'])
        .then((data) => {
            bdays = data.birthdays || [];
            bdays.push({ "name": name, "birthdate": date });

            return setBirthdays(bdays);
        });
}
