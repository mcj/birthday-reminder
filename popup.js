var imgs = [
    'champagne_icon-32.png',
    'stars_icon-32.png',
    'cake_icon-32.png',
    'balloon_icon-32.png',
    'confetti_icon-32.png',
    'confetti2_icon-32.png',
    'crown_icon-32.png',
    'gift_icon-32.png',
    'hat_icon-32.png',
    'mask_icon-32.png',
    'music_icon-32.png',
];

function init() {
    localizeHtmlPage();

    var bdayList = document.getElementById('list-container');

    getBirthdays().then((result) => {
        bdays = result.birthdays;

        if (bdays == undefined || bdays.length == 0) {
            bdayList.appendChild(document.createTextNode("Es wurden noch keine Geburtstage gespeichert."));
            return;
        }

        bdays.sort(function(a,b){
            birthdayA = getNextBirthdayDate(a.birthdate.substring(5));
            birthdayB = getNextBirthdayDate(b.birthdate.substring(5));

            return birthdayA - birthdayB;
        });

        for (let i in bdays) {
            var birthday = new Date(bdays[i].birthdate);
            bdayList.appendChild(constructBirthdayDiv(bdays[i].name, birthday));
        }
    });
}

function getNextBirthdayDate(incompleteDate) {
    today = new Date();
    today.setHours(0, 0, 0, 0);

    year = today.getFullYear();
    if (new Date(year + "-" + incompleteDate) < today) year++;

    return new Date(year + "-" + incompleteDate);
}

function getBirthdays() {
    return chrome.storage.sync.get(['birthdays']);
}

function yearsAgo(date) {
    var diffMs = Date.now() - (24 * 60 * 60 * 1000) - date;
    var ageDate = new Date(diffMs); // milliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function removeBirthday(name, birthday, birthdayItem) {
    if (confirm(chrome.i18n.getMessage("confirm_delete", [name]))) {
        deleteBirthday(name, birthday.toISOString().slice(0, 10))
            .then(() => {
                birthdayItem.remove();
            });
    }
}

function constructBirthdayDiv(name, birthday) {
    var age = yearsAgo(birthday) + 1;

    var nameDiv = document.createElement('div');
    var nameText = chrome.i18n.getMessage("birthday_line", [name, age]);
    nameDiv.appendChild(document.createTextNode(nameText));

    var displayDate = birthday.getDate() + '.' + (birthday.getMonth() + 1) + '.';
    var dateDiv = document.createElement('div');
    dateDiv.classList.add('birthday-date');
    dateDiv.appendChild(document.createTextNode(displayDate));

    var icon = document.createElement('img');
    icon.src = imgs[Math.floor(Math.random() * 11)];
    dateDiv.appendChild(icon);

    var deleteBtn = document.createElement('button');
    deleteBtn.appendChild(document.createTextNode("X"));
    deleteBtn.classList.add("delete-btn");
    deleteBtn.addEventListener('click', (e) => removeBirthday(name, birthday, birthdayItem));

    dateDiv.appendChild(deleteBtn);

    var birthdayItem = document.createElement('div');
    birthdayItem.classList.add('birthday-item');

    birthdayItem.appendChild(nameDiv);
    birthdayItem.appendChild(dateDiv);

    return birthdayItem;
}

window.addEventListener('load', init);
