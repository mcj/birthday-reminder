var CHECK_INTERVAL_MINS = 15;

function birthdayNotification(name, age) {
    chrome.notifications.create({
        "message": chrome.i18n.getMessage("notification_content", [name, age]),
        "type": "basic",
        "iconUrl": chrome.runtime.getURL("cake_icon-128.png"),
        "title": chrome.i18n.getMessage("notification_title", [name]),
        "requireInteraction": true
    });
}

chrome.alarms.create("birthday-check", {
    delayInMinutes: 0.1,
    periodInMinutes: CHECK_INTERVAL_MINS
});

chrome.alarms.onAlarm.addListener((alarm) => {
    if(alarm.name != "birthday-check") return;

    console.log('alarm fired: ' + alarm.name);

    chrome.storage.sync.get(['lastCheckDate']).then((result) => {
        var lastCheck = result.lastCheckDate;

        console.log("last checked at " + lastCheck);

        var todayStart = new Date();
        todayStart.setHours(0, 0, 0, 0);
        if (! lastCheck || new Date(lastCheck) < todayStart) {
            chrome.storage.sync.set({ 'lastCheckDate': new Date().toISOString().slice(0, 10) });
            getBirthdays().then((result) => notifyAboutBirthdaysToday(result.birthdays));
        }
    });
});

console.log("Loaded service worker");

// --- Pfusch! --- //
function getBirthdays() {
    return chrome.storage.sync.get(['birthdays']);
}

function birthdayAge(date) {
    var diffMs = Date.now() - (24 * 60 * 60 * 1000) - date;
    var ageDate = new Date(diffMs); // milliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970) + 1;
}

function notifyAboutBirthdaysToday(bdays) {
    today = new Date();
    today.setHours(0, 0, 0, 0);

    for (let i in bdays) {
        var birthday = new Date(bdays[i].birthdate);
        var nextBirthday = getNextBirthdayDate(bdays[i].birthdate.substring(5));
        nextBirthday.setHours(0, 0, 0, 0);
        if (nextBirthday.toDateString() == today.toDateString()) {
            birthdayNotification(bdays[i].name, birthdayAge(birthday));
        }
    }
}

function getNextBirthdayDate(incompleteDate) {
    today = new Date();
    today.setHours(0, 0, 0, 0);

    year = today.getFullYear();
    if (new Date(year + "-" + incompleteDate) < today) year++;

    return new Date(year + "-" + incompleteDate);
}
