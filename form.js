async function saveBirthday() {
    var name = document.getElementById("nameinput").value;
    var date = document.getElementById("dateinput").value;

    var msg = document.getElementById("errormessage");

    if (name.length <= 0 || date.length <= 0) {
        msg.innerHTML = chrome.i18n.getMessage("error_both_fields_required");
        return;
    }

    if (new Date(date) > new Date()) {
        msg.innerHTML = chrome.i18n.getMessage("error_date_in_future");
        return;
    }

    msg.innerHTML = "";

    addBirthday(name, date)
        .then(() => {
            window.location = "popup.html";
        });
}

function init() {
    localizeHtmlPage();

    document.getElementById("submit-btn").addEventListener('click', saveBirthday);
}

window.addEventListener('load', init);
