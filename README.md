# Birthday reminder chrome extension

![Screenshot of the extension popup](Screenshot-en.png)

## Installation

1. Download the code. In case of a .zip, extract the file
1. In the address bar of Chrome, type chrome://extensions
1. Activate developer mode (top right corner)
1. Click 'Load unpacked' and select the folder with the source code
1. Optionally, pin the extension to the extension bar
